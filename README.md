# wikichanges-watcher

Demonstration of the [wikichanges](https://www.npmjs.com/package/wikichanges)
npm package.

## Usage

Tested on Ubuntu 14.04 with [Node Version
Manager](https://github.com/creationix/nvm).

1. `nvm install 0.10.43` (other versions may also work)
1. `npm -g install npm@latest-2` (may not be necessary)
1. `npm -g install wikichanges-watcher`
1. `wikichanges-watcher`

After a few seconds you should start to see lines printed to the console like
this:

```
TIMESTAMP JSONBLOB
TIMESTAMP JSONBLOB
TIMESTAMP JSONBLOB
```

`TIMESTAMP` is milliseconds since the epoch, and is followed by a space.
`JSONBLOB` is json text, and continues to the end of the line.

For example:

```
1459318885127 {"channel":"#en.wikipedia","flag":"create","page":"Special:Log/newusers","pageUrl":"http://en.wikipedia.org/wiki/Special:Log/newusers","url":"","delta":null,"comment":"New user account","wikipedia":"English Wikipedia","wikipediaUrl":"http://en.wikipedia.org","wikipediaShort":"en","wikipediaLong":"English Wikipedia","user":"Shutupdumdum","userUrl":"http://en.wikipedia.org/wiki/User:Shutupdumdum","unpatrolled":false,"newPage":false,"robot":false,"anonymous":false,"namespace":"special"}
1459318885220 {"channel":"#en.wikipedia","flag":"","page":"Talk:List of forts","pageUrl":"http://en.wikipedia.org/wiki/Talk:List_of_forts","url":"https://en.wikipedia.org/w/index.php?diff=712629509&oldid=590171997","delta":132,"comment":"","wikipedia":"English Wikipedia","wikipediaUrl":"http://en.wikipedia.org","wikipediaShort":"en","wikipediaLong":"English Wikipedia","user":"Ladyrose66","userUrl":"http://en.wikipedia.org/wiki/User:Ladyrose66","unpatrolled":false,"newPage":false,"robot":false,"anonymous":false,"namespace":"talk"}
1459318885766 {"channel":"#en.wikipedia","flag":"thank","page":"Special:Log/thanks","pageUrl":"http://en.wikipedia.org/wiki/Special:Log/thanks","url":"","delta":null,"comment":"SteveStrummer thanked Cwmhiraeth","wikipedia":"English Wikipedia","wikipediaUrl":"http://en.wikipedia.org","wikipediaShort":"en","wikipediaLong":"English Wikipedia","user":"SteveStrummer","userUrl":"http://en.wikipedia.org/wiki/User:SteveStrummer","unpatrolled":false,"newPage":false,"robot":false,"anonymous":false,"namespace":"special"}
```
